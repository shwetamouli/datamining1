__author__ = 'shwetamouli'
import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    if len(record) == 3:
        mr.emit_intermediate(record[2], record[1])
    elif len(record) == 4:
        mr.emit_intermediate(record[1], [record[2], record[3]])
        #mr.emit_intermediate(record[1], record[3])

    # for friend in value:
    #     pair_friends = [key, friend]
    #     pair_friends.sort()
    #     mr.emit_intermediate(tuple(pair_friends), 1)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    avg = 0
    sum = 0
    count = 0
    name = None
    for value in list_of_values:
        if len(value) == 2:
            sum += value[1]
            count += 1
        if isinstance(value, unicode):
            name = value
    avg = float(sum)/float(count)
    for value in list_of_values:
        if len(value) == 2:
            mr.emit((name, key, key, value[0], value[1]))
    mr.emit((name, float(avg)))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


