__author__ = 'shwetamouli'
import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    words = value.split()
    sizes = dict()
    sizes["large"] = 0
    sizes["medium"] = 0
    sizes["small"] = 0
    sizes["tiny"] = 0
    for w in words:
        if len(w) == 1:
            sizes["tiny"] += 1

        elif len(w) > 1 and len(w) < 5:
            sizes["small"] += 1

        elif len(w) > 4 and len(w) < 10:
            sizes["medium"] += 1

        else:
            sizes["large"] += 1

    mr.emit_intermediate(key, sizes)

def reducer(key, list_of_values):
    list1 = list()
    for key1 in list_of_values[0]:
        list1.append([key1, list_of_values[0][key1]])
    list1.sort()
    mr.emit((key, list1))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
