__author__ = 'shwetamouli'
import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    for friend in value:
        pair_friends = [key, friend]
        pair_friends.sort()
        friend_key = pair_friends[0] + pair_friends[1]
        for lv1_friend in record[1]:
            mr.emit_intermediate(friend_key, lv1_friend)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    mutual_friends = list()
    for v in list_of_values:
        if list_of_values.count(v) == 2:
            mutual_friends.append(v)
    mf_set = set(mutual_friends)
    if len(mutual_friends) > 0:
        mr.emit((key, list(mf_set)))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


